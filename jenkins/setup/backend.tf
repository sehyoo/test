terraform {
  backend "s3" {
    bucket = "sehyoo96-node-aws-jenkins-terraform"
    key    = "jenkins.terraform.tfstate"
    region = "eu-west-1"
  }
}

